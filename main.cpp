#include <iostream>
#include "tree.cpp"

using namespace std;

int main()
{
    Drzewo <int> d1;
    try {
    d1.addRoot(100); //dodanie elementu do korzenia
    //dodanie do synów korzenia
    d1.add(2,d1.root());
    d1.add(5,d1.root());
    d1.add(7,d1.root());
    //dodanie do czterech synów trzeciego syna korzenia
    d1.add(8,d1.root()->visit(2));
    d1.add(50,d1.root()->visit(2));
    d1.add(78,d1.root()->visit(2));
    d1.add(34,d1.root()->visit(2));
   //do synów drugiego syna korzenia
    d1.add(65,d1.root()->visit(1));
    d1.add(99,d1.root()->visit(1));
    //i do synów pierwszego syna korzenia
    d1.add(125,d1.root()->visit(0));
    d1.add(98,d1.root()->visit(0));

    cout << "***************** Zawartosc drzewa: ******************" << endl;
    d1.printInElement(d1.root());  //wypisanie korzenie wraz z synami
    d1.printInElement(d1.root()->visit(0)); //wypisanie pierwszego syna korzenia z synami
    d1.printInElement(d1.root()->visit(1));// drugiego
    d1.printInElement(d1.root()->visit(2));//trzeciego
    cout << "Wysokosc drzewa: " << height(d1,d1.root()) << endl;
    cout << "Czy puste: " << d1.isEmpty() << endl;
    cout << "Ilosc elementow: " << d1.size() << endl << endl;


    cout << "Przejscie drzewa PreOrder: ";
    printPreOrder(d1,d1.root());
    cout << endl;
    cout << "Przejscie drzewa PostOrder: ";
    printPostOrder(d1,d1.root());
    cout << endl;



    cout << endl << endl << "Usuniecie elementu 50 oraz 98." << endl;
    d1.remove(d1.root()->visit(2)->visit(1));
    d1.remove(d1.root()->visit(0)->visit(1));
    d1.printInElement(d1.root()->visit(2));
    d1.printInElement(d1.root()->visit(0));
    cout << "Ilosc elementow: " << d1.size() << endl;

    cout << endl << endl << "Dodanie elementu do 125. " << endl;
    d1.add(23,d1.root()->visit(0)->visit(0));
    d1.printInElement(d1.root()->visit(0)->visit(0));
    cout << "Wysokosc drzewa: " << height(d1,d1.root()) << endl;



    }
    catch (exception & e) {
        cerr << e.what() << endl;
    }

    return 0;
}



