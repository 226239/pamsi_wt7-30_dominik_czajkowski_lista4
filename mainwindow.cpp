#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "kolejka_priorytetowa.cpp"
PQ <int> s;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

//przycisk dodania do kolejki
void MainWindow::on_add_button_clicked()
{
    s.dodaj(ui->input_cnt->text().toInt(),ui->key_in->text().toInt());
}
//przycisk wyświetlenia kolejki
void MainWindow::on_disp_btt_clicked()
{
    s.list();
}
//przycisk zdjęcia elementu z kolejki
void MainWindow::on_pushButton_clicked()
{
    ui->del_label->setText(QString::number(s.put().val));
}

//klawisz wypełnienia tablicy losowymi wartościami a następnie sortowania jej
void MainWindow::on_pushButton_2_clicked()
{
    int tb[20];
    for(unsigned int i=0; i<20;i++)
        tb[i]=rand() % 30;
    qDebug() << "Tablica przed sortowaniem:";
    for(unsigned int i=0; i<20;i++)
        qDebug() << tb[i];
    PQ <int> tmp;
    for(unsigned int i=0; i<20;i++)
        tmp.dodaj(0,tb[i]);
    for(unsigned int i=0; i<20;i++)
        tb[i] = tmp.put().key;
    qDebug() << "Tablica po sortowaniu:";
    for(unsigned int i=0; i<20;i++)
        qDebug() << tb[i];



}